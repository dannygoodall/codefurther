CodeFurther Package Documentation v |global_version_full|
=========================================================

.. sectionauthor:: Danny Goodall <danny@onebloke.com>

.. warning::
    This documentation is currently out of sync with the functionality in the **CodeFurther** package. In addition
    to the functionality described in this documentation, the following data sources have also been implemented:

    * ``football``
    * ``weather``

    On top of this, the following means of sharing have been implemented:

    * ``email``
    * ``twitter``
    * ``slack``
    * ``textmessage``

    Many of these services require the presence of a ``cf_setting.py`` script that contains the following settings:

    * ``CF_TWITTER_APP_KEY``
    * ``CF_TWITTER_APP_SECRET``
    * ``CF_TWITTER_OAUTH_TOKEN``
    * ``CF_TWITTER_OAUTH_TOKEN_SECRET``
    * ``CF_SLACK_API_KEY``
    * ``CF_SMTP_SERVER``
    * ``CF_SMTP_PORT``
    * ``CF_SMTP_USERNAME``
    * ``CF_SMTP_PASSWORD``
    * ``CF_TWILIO_ACCOUNT``
    * ``CF_TWILIO_TOKEN``
    * ``CF_TWILIO_FROM``
    * ``CF_FORECASTIO_API_KEY``

    If this script file is not found, then the corresponding environment variable will be searched for.

    The entire **CodeFurther** package is under development, so proceed

Contents
--------
.. toctree::
   :maxdepth: 3
   :includehidden:

   installation
   top40
   lyrics
   directions
   utils
   errors
   changes

.. include:: ../README.rst


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

